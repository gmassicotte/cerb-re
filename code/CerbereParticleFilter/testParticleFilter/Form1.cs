﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CerbereParticleFilter;
using CerbereParticleFilter.Particule_Filter;
using CerbereParticleFilter.Data_Acquisition;
using CerbereParticleFilter.BaseTypes;
using CerbereParticleFilter.Particule_Filter.Dynamic_Model;
using CerbereParticleFilter.Particule_Filter.Static_Model;

namespace testParticleFilter
{
    public partial class Form1 : Form
    {
        /***************** param World *******************/
        const int m_NbFeatures = 175;

        const int m_SizeX = 100;
        const int m_SizeY = 100;
        const int m_SizeZ = 100;

        Vector3 m_InitialRobotPos = new Vector3(25,0,30);
        const double m_InitialOrientation = 0;
        /**************** param Camera *******************/
        const int m_XNbPix = 640;
        const int m_YNbPix = 480;

        const double m_XAngleMin = -28.5 / 180 * Math.PI;
        const double m_XAngleMax = 28.5 / 180 * Math.PI;
        const double m_YAngleMin = -21.5 / 180 * Math.PI;
        const double m_YAngleMax = 21.5 / 180 * Math.PI;
        /***************** param Filter *******************/
        const int m_NbRobotPosParticles = 500;
        const int m_NbFeaturePosParticles = 500;

        const double m_RobotPosResamplingTreshold = 5.5;
        const double m_FeaturePosResamplingTreshold = .04;
        /**************** param Commandes *****************/
        const double m_RSCRot = Math.PI/180;
        const double m_RSCForward = .01;

        const double m_SCRot = Math.PI / 150;
        const double m_SCForward = .03;

        const double m_MaxRot = .15;
        const double m_MaxForward = 1;
        /**************************************************/

        World m_World;
        LocalSLAMParticuleFilter m_ParticleFilter;
        CapteurAngle m_Camera;

        Queue<Commande> m_CommandeQueue;
        Commande m_CurCommande;


        Bitmap m_BackBuffer;
        Graphics m_BackBufferDrawer;
        Graphics m_G;
        public Form1()
        {
            InitializeComponent();
            m_BackBuffer = new Bitmap(m_SizeX*4, m_SizeZ*4);
            m_BackBufferDrawer = Graphics.FromImage(m_BackBuffer);
            m_G = CreateGraphics();

            m_CurCommande = null;
            m_CommandeQueue = new Queue<Commande>();

            m_World = new World(m_NbFeatures, m_SizeX, m_SizeY, m_SizeZ, m_InitialRobotPos, m_InitialOrientation);

            m_Camera = new CapteurAngle(m_World, m_XNbPix, m_YNbPix, m_XAngleMin, m_XAngleMax, m_YAngleMin, m_YAngleMax);
            m_ParticleFilter = new LocalSLAMParticuleFilter(m_NbRobotPosParticles, m_NbFeaturePosParticles, m_Camera, m_RobotPosResamplingTreshold, m_FeaturePosResamplingTreshold, m_InitialRobotPos, m_InitialOrientation);

            m_ParticleFilter.run(0, 0.001, 0, 0.001);
        }

        public void queueCommande(double X, double Z)
        {
            double CRot = Math.Atan2(X - m_World.RobotState.Pos.X, Z - m_World.RobotState.Pos.Z) - m_World.RobotState.Orientation;
            double CForward = Math.Sqrt(Math.Pow(X-m_World.RobotState.Pos.X,2) + Math.Pow(Z-m_World.RobotState.Pos.Z,2));

            m_CommandeQueue.Enqueue(new Commande(CRot, CForward));
        }

        public void runCommande()
        {
            if (m_CurCommande == null)
                if (m_CommandeQueue.Count > 0)
                    m_CurCommande = m_CommandeQueue.Dequeue();
                else
                    return;

            Commande SubCommande = m_CurCommande.getSubCommande(m_MaxRot, m_MaxForward);
            if (SubCommande == null)
            {
                m_CurCommande = null;
                return;
            }

            m_World.RobotState.Orientation += MathHelper.GaussianRandom(RandomHandler.R, SubCommande.CRot, m_RSCRot);
            double Forward = MathHelper.GaussianRandom(RandomHandler.R, SubCommande.CForward, m_RSCForward);
            m_World.RobotState.Pos.X += Math.Sin(m_World.RobotState.Orientation) * Forward;
            m_World.RobotState.Pos.Z += Math.Cos(m_World.RobotState.Orientation) * Forward;

            m_ParticleFilter.run(SubCommande.CRot, m_SCRot, SubCommande.CForward, m_SCForward);
        }

        
        public void DrawState()
        {
            m_BackBufferDrawer.Clear(Color.White);
            foreach (Feature f in m_World.Features)
                m_BackBufferDrawer.FillEllipse(new SolidBrush(Color.Blue), (float)f.Pos.X * 4, (m_SizeZ - (float)f.Pos.Z) * 4, 3f, 3f);
            foreach (Feature f in m_ParticleFilter.Features)
                m_BackBufferDrawer.FillEllipse(new SolidBrush(Color.Red), (float)f.Pos.X * 4, (m_SizeZ - (float)f.Pos.Z) * 4, 3f, 3f);

            m_BackBufferDrawer.FillRectangle(new SolidBrush(Color.Green), (float)m_World.RobotState.Pos.X * 4, (m_SizeZ - (float)m_World.RobotState.Pos.Z) * 4, 3f, 3f);
            m_BackBufferDrawer.FillRectangle(new SolidBrush(Color.Orange), (float)m_ParticleFilter.RobotState.Pos.X * 4, (m_SizeZ - (float)m_ParticleFilter.RobotState.Pos.Z) * 4, 3f, 3f);

            m_G.DrawImage(m_BackBuffer, 0, 0);
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            Text = "X : " + e.X/4 + "; Y : " + e.Y/4;
            queueCommande(e.X / 4, m_SizeZ - e.Y / 4);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            runCommande();
            DrawState();
        }
    }
}
