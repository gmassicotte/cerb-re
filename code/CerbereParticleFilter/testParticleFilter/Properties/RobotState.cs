﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CerbereParticleFilter.BaseTypes;

namespace CerbereParticleFilter
{
    public class RobotState : ICloneable<RobotState>
    {
        public Vector3 Pos
        {
            get { return m_Pos; }
            set { m_Pos = value; }
        }
        public double Orientation
        {
            get { return m_Orientation; }
            set { m_Orientation = value; }
        }

        Vector3 m_Pos;
        double m_Orientation;

        public RobotState(Vector3 pos, double orientation)
        {
            m_Pos = pos;
            m_Orientation = orientation;
        }

        public RobotState clone()
        {
            return new RobotState(m_Pos.clone(), m_Orientation);
        }
    }
}
