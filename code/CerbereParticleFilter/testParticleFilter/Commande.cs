﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace testParticleFilter
{
    public class Commande
    {
        public double CRot
        {
            get { return m_CRot; }
            set { m_CRot = value; }
        }
        public double CForward
        {
            get { return m_CForward; }
            set { m_CForward = value; }
        }

        double m_CRot;
        double m_CForward;

        public Commande(double cRot, double cForward)
        {
            m_CRot = cRot;
            m_CForward = cForward;
        }

        public Commande getSubCommande(double maxAngle, double maxForward)
        {
            if (m_CRot != 0)
                if (m_CRot > 0)
                    if (m_CRot <= maxAngle)
                    {
                        m_CRot = 0;
                        return new Commande(m_CRot, 0);
                    }
                    else
                    {
                        m_CRot = m_CRot - maxAngle;
                        return new Commande(maxAngle, 0);
                    }
                else
                    if (m_CRot >= -maxAngle)
                    {
                        m_CRot = 0;
                        return new Commande(m_CRot, 0);
                    }
                    else
                    {
                        m_CRot = m_CRot + maxAngle;
                        return new Commande(-maxAngle, 0);
                    }
            else if (m_CForward != 0)
                if (m_CForward > 0)
                    if (m_CForward <= maxForward)
                    {
                        m_CForward = 0;
                        return new Commande(0, m_CForward);
                    }
                    else
                    {
                        m_CForward = m_CForward - maxForward;
                        return new Commande(0, maxForward);
                    }
                else
                    if (m_CForward >= -maxForward)
                    {
                        m_CForward = 0;
                        return new Commande(0, m_CForward);
                    }
                    else
                    {
                        m_CForward = m_CForward + maxForward;
                        return new Commande(0, -maxForward);
                    }
            else
                return null;
        }
    }
}
