﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CerbereParticleFilter.BaseTypes;

namespace CerbereParticleFilter
{
    public class Feature : ICloneable<Feature>
    {
        public ulong Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        public Vector3 Pos
        {
            get { return m_Pos; }
            set { m_Pos = value; }
        }


        ulong m_Id;

        Vector3 m_Pos;

        public Feature(Vector3 pos)
        {
            m_Pos = pos;
            m_Id = IdGenerator.getID();
        }

        public Feature(Vector3 pos, ulong id)
        {
            m_Pos = pos;
            m_Id = id;
        }

        public Feature clone()
        {
            return new Feature(m_Pos.clone(), m_Id);
        }
    }

    public class FeatureReading
    {
        public ulong Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        public double OrientationAngle
        {
            get { return m_OrientationAngle; }
            set { m_OrientationAngle = value; }
        }
        public double ElevationAngle
        {
            get { return m_ElevationAngle; }
            set { m_ElevationAngle = value; }
        }


        ulong m_Id;

        double m_OrientationAngle;
        double m_ElevationAngle;

        public FeatureReading(double orientationAngle, double elevationAngle, ulong id)
        {
            m_OrientationAngle = orientationAngle;
            m_ElevationAngle = elevationAngle;
            m_Id = id;
        }
    }
}
