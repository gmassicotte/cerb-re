﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CerbereParticleFilter.Particule_Filter.Static_Model;
using CerbereParticleFilter.Particule_Filter.Dynamic_Model;
using CerbereParticleFilter.BaseTypes;

namespace CerbereParticleFilter.Particule_Filter
{
    public class Particule<T> where T : ICloneable<T>
    {
        public double Weight
        {
            get { return m_Weight; }
            set { m_Weight = value; }
        }
        public T Data
        {
            get { return m_Data; }
            set { m_Data = value; }
        }

        protected double m_Weight;
        protected T m_Data;

        public Particule(T data, double weight)
        {
            m_Weight = weight;
            m_Data = data;
        }
        public Particule<T> Create(T data, double weight)
        {
            return new Particule<T>(data, weight);
        }

        public void UpdateWeight(double newWeight)
        {
            Weight = newWeight;
        }
    }

    public abstract class ParticuleSet<T, U> where T : Particule<U> where U : ICloneable<U>
    {
        public T this[int I]
        {
            get
            {
                return m_Parts[I];
            }
        }
        public int Count
        {
            get
            {
                return m_Parts.Count;
            }
        }

        public virtual double SumPoids
        {
            get
            {
                if (m_SumPoidsChanged)
                {
                    m_SumPoidsChanged = false;
                    return (m_SumPoids = getSumPoids());
                }
                else
                    return m_SumPoids;
            }
        }
        protected bool m_SumPoidsChanged = true;
        protected double m_SumPoids;
        public virtual U Avg
        {
            get
            {
                if (m_AvgChanged)
                {
                    m_AvgChanged = false;
                    return (m_Avg = getAvg());
                }
                else
                    return m_Avg;
            }
        }
        protected bool m_AvgChanged = true;
        protected U m_Avg;
        public virtual U Sig
        {
            get
            {
                if (m_SigChanged)
                {
                    m_SigChanged = false;
                    return (m_Sig = getSig());
                }
                else
                    return m_Sig;
            }
        }
        protected bool m_SigChanged = true;
        protected U m_Sig;

        protected abstract U getAvg();
        protected abstract U getSig();

        protected List<T> m_Parts;
        public ParticuleSet()
        {
            m_Parts = new List<T>();
        }

        public virtual double getWeight(int index)
        {
            return m_Parts[index].Weight;
        }
        public virtual void UpdateWeight(int index, double newWeight)
        {
            m_AvgChanged = true;
            m_SigChanged = true;
            m_SumPoidsChanged = true;
            m_Parts[index].UpdateWeight(newWeight);
        }
        public virtual void Normalize()
        {
            double sumPoids = SumPoids;
            foreach (Particule<U> part in m_Parts)
                part.UpdateWeight(part.Weight / sumPoids);
            m_SumPoids = 1;
        }
        public virtual void Add(T newWeight)
        {
            m_AvgChanged = true;
            m_SigChanged = true;
            m_SumPoidsChanged = true;
            m_Parts.Add(newWeight);
        }
        public virtual double CalcNeff()
        {
            double Res = 0;
            foreach (Particule<U> part in m_Parts)
                Res += Math.Pow(part.Weight, 2);
            return Res;
        }
        public virtual void Resample()
        {
            List<T> oldParts = m_Parts;
            m_Parts = new List<T>();

            double[] index = new double[oldParts.Count + 1];
            for (int I = 0; I < index.Length - 1; ++I)
                index[I + 1] = index[I] + oldParts[I].Weight;
            index[index.Length - 1] = 1;

            List<double> ParticleID = new List<double>();
            for (int I = 0; I < index.Length; ++I)
                ParticleID.Add(RandomHandler.R.NextDouble());
            ParticleID.Sort();

            int IdxList = 0, IdxIndex = 1;
            while (IdxList < index.Length)
            {
                if (ParticleID[IdxList] < index[IdxIndex])
                {
                    m_Parts.Add(Create(oldParts[IdxIndex - 1].Data.clone(), 0));
                    ++IdxList;
                }
                else
                    ++IdxIndex;
            }

            for (int K = m_Parts.Count - 1; K >= 0; --K)
                m_Parts[K].UpdateWeight(1.0 / m_Parts.Count);
        }
        public abstract void Sim(double CRot, double sCRot, double CForward, double sCForward);
        public abstract T Create(U data, double weight);

        private double getSumPoids()
        {
            double Res = 0;
            foreach (Particule<U> p in m_Parts)
                Res += p.Weight;
            return Res;
        }
    }

    public class FeaturePosParticuleSet : ParticuleSet<FeatureParticule, Feature>
    {
        public int Age
        {
            get { return m_Age; }
            set { m_Age = value; }
        }
        public FeatureReading InitialReading
        {
            get { return m_InitialReading; }
            set { m_InitialReading = value; }
        }
        public RobotState InitialReadingPos
        {
            get { return m_InitialReadingState; }
            set { m_InitialReadingState = value; }
        }

        int m_Age;
        FeatureReading m_InitialReading;
        FeatureReading m_CurReading;
        RobotState m_InitialReadingState;
        RobotState m_CurReadingState;
        
        double m_ResamplingTreshold;

        public FeaturePosParticuleSet(FeatureReading initialReading, RobotState readingState, double resamplingTreshold)
        {
            m_Age = 1;
           
            m_InitialReading = initialReading;
            m_CurReading = initialReading;
            m_InitialReadingState = readingState;
            m_CurReadingState = readingState;
            m_ResamplingTreshold = resamplingTreshold;
        }
        public void setCurReading(FeatureReading reading, RobotState readingState)
        {
            m_CurReading = reading;
            m_CurReadingState = readingState;
        }

        protected override Feature getAvg()
        {
            Vector3 Res = new Vector3(0,0,0);
            ulong Id = ulong.MaxValue;
            foreach (FeatureParticule p in m_Parts)
            {
                Id = p.Feature.Id;
                Res.X += p.Feature.Pos.X * p.Weight;
                Res.Y += p.Feature.Pos.Y * p.Weight;
                Res.Z += p.Feature.Pos.Z * p.Weight;
            }
            Res.X /= SumPoids;
            Res.Y /= SumPoids;
            Res.Z /= SumPoids;

            if (m_Parts.Count > 0)
                return new Feature(Res, Id);
            else
                return null;
        }
        protected override Feature getSig()
        {
            Vector3 Res = new Vector3(0, 0, 0);
            ulong Id = ulong.MaxValue;
            foreach (FeatureParticule p in m_Parts)
            {
                Id = p.Feature.Id;
                Res.X += Math.Pow(p.Feature.Pos.X - Avg.Pos.X, 2) * p.Weight;
                Res.Y += Math.Pow(p.Feature.Pos.Y - Avg.Pos.Y, 2) * p.Weight;
                Res.Z += Math.Pow(p.Feature.Pos.Z - Avg.Pos.Z, 2) * p.Weight;
            }
            Res.X = Math.Sqrt(Res.X / SumPoids);
            Res.Y = Math.Sqrt(Res.Y / SumPoids);
            Res.Z = Math.Sqrt(Res.Z / SumPoids);

            if (m_Parts.Count > 0)
                return new Feature(Res, Id);
            else
                return null;
        }
        public override void Sim(double CRot, double sCRot, double CForward, double sCForward) { }
        public override FeatureParticule Create(Feature data, double weight)
        {
            return new FeatureParticule(data, weight);
        }

        public override void Resample()
        {
            double Neff = CalcNeff();
            if ((Neff > m_ResamplingTreshold) && false)
            {
                List<FeatureParticule> newRobotStates = new List<FeatureParticule>();
                if ((Age == 1))
                {
                    ++Age;
                    Feature sig = Sig;

                    double O1 = m_InitialReading.OrientationAngle + m_InitialReadingState.Orientation;
                    double O2 = m_CurReading.OrientationAngle + m_CurReadingState.Orientation;

                    Vector3 v1 = new Vector3(Math.Sin(O1), Math.Sin(m_InitialReading.ElevationAngle), Math.Cos(O1));
                    Vector3 v2 = new Vector3(Math.Sin(O2), Math.Sin(m_CurReading.ElevationAngle), Math.Cos(O2));

                    //test
                    /*v1 = new Vector3(2, 3, 4);
                    v2 = new Vector3(4, 3, 1);*/

                    Vector3 Normale = Vector3.CrossProduct(ref v1, ref v2);

                    //test
                    /*Vector3 P1 = new Vector3(7, 11, 13) + Normale;
                    Vector3 P2 = new Vector3(9, 11, 10) - Normale;
                    Vector3 P1P2 = P2 - P1;*/

                    Vector3 P1P2 = m_CurReadingState.Pos - m_InitialReadingState.Pos;
                    Vector3 Dmin = (Vector3.DotProduct(ref P1P2, ref Normale) / Normale.SquareLength) * Normale;
                    P1P2 -= Dmin;
                    Vector3 P1P3 = P1P2 - (Vector3.DotProduct(ref P1P2, ref v2) / v2.SquareLength) * v2;
                    Vector3 PIntersect = m_InitialReadingState.Pos + P1P3.SquareLength / Vector3.DotProduct(ref v1, ref P1P3) * v1;

                    //test
                    //PIntersect = P1 + P1P3.SquareLength / Vector3.DotProduct(ref v1, ref P1P3) * v1 + .5 * Dmin;
                    

                    for (int I = m_Parts.Count; I > 0; --I)
                    {
                        Feature newFeature = new Feature(new Vector3(
                                    MathHelper.GaussianRandom(RandomHandler.R, PIntersect.X, sig.Pos.X),
                                    MathHelper.GaussianRandom(RandomHandler.R, PIntersect.Y, sig.Pos.Y),
                                    MathHelper.GaussianRandom(RandomHandler.R, PIntersect.Z, sig.Pos.Z)), m_InitialReading.Id);

                        newRobotStates.Add(new FeatureParticule(newFeature,
                            MathHelper.Gauss(PIntersect.X, sig.Pos.X, newFeature.Pos.X) * MathHelper.Gauss(PIntersect.Y, sig.Pos.Y, newFeature.Pos.Y) * MathHelper.Gauss(PIntersect.Z, sig.Pos.Z, newFeature.Pos.Z)));
                    }

                    m_Parts = newRobotStates;
                    m_SumPoidsChanged = true;
                    m_AvgChanged = true;
                    m_SigChanged = true;
                    Normalize();
                }
                else
                {
                    List<FeatureParticule> oldParts = m_Parts;
                    Feature sig = Sig;
                    m_Parts = new List<FeatureParticule>();
                    m_AvgChanged = true;
                    m_SigChanged = true;
                    m_SumPoidsChanged = true;

                    double[] index = new double[oldParts.Count + 1];
                    for (int I = 0; I < index.Length - 1; ++I)
                        index[I + 1] = index[I] + oldParts[I].Weight;
                    index[index.Length - 1] = 1;

                    List<double> ParticleID = new List<double>();
                    for (int I = 0; I < index.Length; ++I)
                        ParticleID.Add(RandomHandler.R.NextDouble());
                    ParticleID.Sort();

                    int IdxList = 0, IdxIndex = 1;
                    while (IdxList < index.Length)
                    {
                        if (ParticleID[IdxList] < index[IdxIndex])
                        {
                            Feature temp = oldParts[IdxIndex - 1].Data.clone();
                            temp.Pos.X += MathHelper.GaussianRandom(RandomHandler.R, 0, sig.Pos.X);
                            temp.Pos.Y += MathHelper.GaussianRandom(RandomHandler.R, 0, sig.Pos.Y);
                            temp.Pos.Z += MathHelper.GaussianRandom(RandomHandler.R, 0, sig.Pos.Z);
                            m_Parts.Add(Create(temp, 0));
                            ++IdxList;
                        }
                        else
                            ++IdxIndex;
                    }

                    for (int K = m_Parts.Count - 1; K >= 0; --K)
                        m_Parts[K].UpdateWeight(1.0 / m_Parts.Count);

                    /*for (int I = m_Parts.Count; I > 0; --I)
                    {
                        Feature newFeature = new Feature(new Vector3(
                                    MathHelper.GaussianRandom(RandomHandler.R, Avg.Pos.X, Sig.Pos.X),
                                    MathHelper.GaussianRandom(RandomHandler.R, Avg.Pos.Y, Sig.Pos.Y),
                                    MathHelper.GaussianRandom(RandomHandler.R, Avg.Pos.Z, Sig.Pos.Z)), m_InitialReading.Id);
                        newRobotStates.Add(new FeatureParticule(newFeature,
                            MathHelper.Gauss(Avg.Pos.X, Sig.Pos.X, newFeature.Pos.X) * MathHelper.Gauss(Avg.Pos.Y, Sig.Pos.Y, newFeature.Pos.Y) * MathHelper.Gauss(Avg.Pos.Z, Sig.Pos.Z, newFeature.Pos.Z)));
                    }

                    m_Parts = newRobotStates;*/
                    
                }
            }
            Normalize();
        }
    }

    public class RobotStateParticuleSet : ParticuleSet<RobotStateParticule, RobotState>
    {
        protected override RobotState getAvg()
        {
            Vector3 Res = new Vector3(0, 0, 0);
            double Orientation = 0;
            foreach (RobotStateParticule p in m_Parts)
            {
                Res.X += p.RobotState.Pos.X * p.Weight;
                Res.Y += p.RobotState.Pos.Y * p.Weight;
                Res.Z += p.RobotState.Pos.Z * p.Weight;
                Orientation += p.RobotState.Orientation * p.Weight;
            }
            Res.X /= SumPoids;
            Res.Y /= SumPoids;
            Res.Z /= SumPoids;
            Orientation /= SumPoids;

            if (m_Parts.Count > 0)
                return new RobotState(Res, Orientation);
            else
                return null;
        }
        protected override RobotState getSig()
        {
            Vector3 Res = new Vector3(0, 0, 0);
            double Orientation = 0;
            foreach (RobotStateParticule p in m_Parts)
            {
                Res.X += Math.Pow(p.RobotState.Pos.X - Avg.Pos.X, 2) * p.Weight;
                Res.Y += Math.Pow(p.RobotState.Pos.Y - Avg.Pos.Y, 2) * p.Weight;
                Res.Z += Math.Pow(p.RobotState.Pos.Z - Avg.Pos.Z, 2) * p.Weight;
                Orientation += Math.Pow(p.RobotState.Orientation - Avg.Orientation, 2) * p.Weight;
            }
            Res.X = Math.Sqrt(Res.X / SumPoids);
            Res.Y = Math.Sqrt(Res.Y / SumPoids);
            Res.Z = Math.Sqrt(Res.Z / SumPoids);
            Orientation = Math.Sqrt(Orientation / SumPoids);

            if (m_Parts.Count > 0)
                return new RobotState(Res, Orientation);
            else
                return null;
        }

        public override void Sim(double CRot, double sCRot, double CForward, double sCForward)
        {
            List<RobotStateParticule> latestRobotStates = m_Parts;
            m_Parts = new List<RobotStateParticule>();

            foreach (RobotStateParticule RSPart in latestRobotStates)
            {
                double Forward = MathHelper.GaussianRandom(RandomHandler.R, CForward, sCForward);
                double Orientation = MathHelper.GaussianRandom(RandomHandler.R, CRot, sCRot);


                double Weight = RSPart.Weight * MathHelper.Gauss(CForward, sCForward, Forward) * MathHelper.Gauss(CRot, sCRot, Orientation);

                RobotState RS = new RobotState(
                    new Vector3(
                        RSPart.RobotState.Pos.X + Forward * Math.Sin(Orientation + RSPart.RobotState.Orientation),
                        RSPart.RobotState.Pos.Y,
                        RSPart.RobotState.Pos.Z + Forward * Math.Cos(Orientation + RSPart.RobotState.Orientation)),

                    Orientation + RSPart.RobotState.Orientation);


                m_Parts.Add(new RobotStateParticule(RS, Weight));
            }
        }

        public override RobotStateParticule Create(RobotState data, double weight)
        {
            return new RobotStateParticule(data, weight);
        }

        /*public override void Resample()
        {
            double Neff = CalcNeff();
            List<RobotStateParticule> newRobotStates = new List<RobotStateParticule>();
            RobotState sig = Sig;

            for (int I = m_Parts.Count; I > 0; --I)
            {
                RobotState newFeature = new RobotState(new Vector3(
                                    MathHelper.GaussianRandom(RandomHandler.R, Avg.Pos.X, sig.Pos.X),
                                    MathHelper.GaussianRandom(RandomHandler.R, Avg.Pos.Y, sig.Pos.Y),
                                    MathHelper.GaussianRandom(RandomHandler.R, Avg.Pos.Z, sig.Pos.Z)), MathHelper.GaussianRandom(RandomHandler.R, Avg.Orientation, sig.Orientation));

                newRobotStates.Add(new RobotStateParticule(newFeature, 1.0 / m_Parts.Count));
                   /* MathHelper.Gauss(Avg.Pos.X, Sig.Pos.X, newFeature.Pos.X) * MathHelper.Gauss(Avg.Pos.Y, Sig.Pos.Y, newFeature.Pos.Y) * MathHelper.Gauss(Avg.Pos.Z, Sig.Pos.Z, newFeature.Pos.Z)));*/
            /*}

           
            m_Parts = newRobotStates;
            m_AvgChanged = true;
            m_SigChanged = true;
            m_SumPoidsChanged = true;
        }*/
    }
}
