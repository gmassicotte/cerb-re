﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CerbereParticleFilter.Particule_Filter.Static_Model
{
    public class FeatureParticule : Particule<Feature>
    {
        public Feature Feature
        {
          get { return m_Data; }
          set { m_Data = value; }
        }


        public FeatureParticule(Feature feature, double weight)
            : base(feature, weight)
        { }
    }
}
