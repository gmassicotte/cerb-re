﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CerbereParticleFilter.BaseTypes;

namespace CerbereParticleFilter.Particule_Filter.Static_Model
{
    public static class CustomResampling
    {
        // À corriger pour considérer les features d'âge 0 et 1
        public static FeaturePosParticuleSet Resample(FeaturePosParticuleSet latestFeatureParticules, double ResamplingTreshold, ulong FeatureId, int nbFeaturePosParts)
        {
            //TODO Intégrer le code pour âge 1

            //TODO See if Treshold is ok
            if (latestFeatureParticules.CalcNeff() < ResamplingTreshold)
            {
                Feature Avg = latestFeatureParticules.Avg;
                Feature Sig = latestFeatureParticules.Sig;

                FeaturePosParticuleSet newFeatureParticules = new FeaturePosParticuleSet(latestFeatureParticules.InitialReading, latestFeatureParticules.InitialReadingPos, ResamplingTreshold);
                for (int I = nbFeaturePosParts; I > 0; --I)
                {
                    Feature newFeature = new Feature(new Vector3(
                                MathHelper.GaussianRandom(RandomHandler.R, Avg.Pos.X, Sig.Pos.X),
                                MathHelper.GaussianRandom(RandomHandler.R, Avg.Pos.Y, Sig.Pos.Y),
                                MathHelper.GaussianRandom(RandomHandler.R, Avg.Pos.Z, Sig.Pos.Z)), FeatureId);
                    newFeatureParticules.Add(new FeatureParticule(newFeature,
                        MathHelper.Gauss(Avg.Pos.X, Sig.Pos.X, newFeature.Pos.X) * MathHelper.Gauss(Avg.Pos.Y, Sig.Pos.Y, newFeature.Pos.Y) * MathHelper.Gauss(Avg.Pos.Z, Sig.Pos.Z, newFeature.Pos.Z)));
                }

                newFeatureParticules.Normalize();

                return newFeatureParticules;
            }
            else
                return latestFeatureParticules;
        }
    }
}
