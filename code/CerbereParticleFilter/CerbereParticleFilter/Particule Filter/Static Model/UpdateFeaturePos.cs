﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CerbereParticleFilter.Particule_Filter.Static_Model
{
    public static class UpdateFeatureParticules
    {
        public static SortedList<ulong, FeaturePosParticuleSet> Update(SortedList<ulong, FeaturePosParticuleSet> latestFeatureParticules, List<FeatureReading> Readings, double[][][][] PartialProbs, double ResamplingTreshold, int nbFeaturePosParts)
        {
            double Sum;
            //SortedList<ulong, FeaturePosParticuleSet> newFeatureParticles = new SortedList<ulong, FeaturePosParticuleSet>();
            for (int I = PartialProbs.Length - 1; I >= 0; --I)
            {
                FeaturePosParticuleSet CurParticleSet = latestFeatureParticules[Readings[I].Id];
                for (int J = PartialProbs[I].Length - 1; J >= 0; --J)
                {
                    Sum = 0;
                    for (int K = PartialProbs[I][J].Length - 1; K >= 0; --K)
                        Sum += PartialProbs[I][J][K][0] * PartialProbs[I][J][K][1];

                    CurParticleSet.UpdateWeight(J, CurParticleSet.getWeight(J) * Sum);
                }

                //CurParticleSet.Normalize();

                CurParticleSet.Resample();
                //newFeatureParticles.Add(Readings[I].Id, CustomResampling.Resample(CurParticleSet, ResamplingTreshold, Readings[I].Id, nbFeaturePosParts));
            }
            return latestFeatureParticules;
        }

        public static SortedList<ulong, FeaturePosParticuleSet> Update2(SortedList<ulong, FeaturePosParticuleSet> latestFeatureParticules, List<FeatureReading> Readings, RobotState curState, double[][][] PartialProbs, double ResamplingTreshold, int nbFeaturePosParts)
        {
            double Sum;
            //SortedList<ulong, FeaturePosParticuleSet> newFeatureParticles = new SortedList<ulong, FeaturePosParticuleSet>();
            for (int I = PartialProbs.Length - 1; I >= 0; --I)
            {
                FeaturePosParticuleSet CurParticleSet = latestFeatureParticules[Readings[I].Id];
                for (int J = PartialProbs[I].Length - 1; J >= 0; --J)
                {
                    Sum = PartialProbs[I][J][0] * PartialProbs[I][J][1];

                    CurParticleSet.UpdateWeight(J, CurParticleSet.getWeight(J) * Sum);
                }

                //CurParticleSet.Normalize();
                CurParticleSet.setCurReading(Readings[I], curState);
                CurParticleSet.Resample();
                //newFeatureParticles.Add(Readings[I].Id, CustomResampling.Resample(CurParticleSet, ResamplingTreshold, Readings[I].Id, nbFeaturePosParts));
            }
            return latestFeatureParticules;
        }
    }
}
