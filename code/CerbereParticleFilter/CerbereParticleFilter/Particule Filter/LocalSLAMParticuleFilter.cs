﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CerbereParticleFilter.Particule_Filter.Static_Model;
using CerbereParticleFilter.Particule_Filter.Dynamic_Model;
using CerbereParticleFilter.Data_Acquisition;
using CerbereParticleFilter.BaseTypes;

namespace CerbereParticleFilter.Particule_Filter
{
    public class LocalSLAMParticuleFilter
    {
        public RobotState RobotState
        {
            get
            {
                return m_RobotPosParticules.Avg;
            }
        }

        public List<Feature> Features
        {
            get
            {
                List<Feature> Features = new List<Feature>();
                foreach (KeyValuePair<ulong, FeaturePosParticuleSet> l in m_FeaturesParticules)
                    Features.Add(l.Value.Avg);
                return Features;
            }
        }

        RobotStateParticuleSet m_RobotPosParticules;
        SortedList<ulong, FeaturePosParticuleSet> m_FeaturesParticules;

        double m_RobotPosResamplingTreshold;
        double m_FeaturePosResamplingTreshold;

        CapteurAngle m_Camera;

        int m_NbRobotPosParts;
        int m_NbFeaturePosParts;
        int[] size = new int[]{100, 100, 100};
        public LocalSLAMParticuleFilter(int nbRobotPosParts, int nbFeaturePosParts, CapteurAngle camera, double robotPosResamplingTreshold, double featurePosResamplingTreshold, Vector3 initialRobotPos, double initialRobotOrientation)
        {
            m_NbRobotPosParts = nbRobotPosParts;
            m_NbFeaturePosParts = nbFeaturePosParts;

            m_RobotPosResamplingTreshold = robotPosResamplingTreshold;
            m_FeaturePosResamplingTreshold = featurePosResamplingTreshold;

            m_Camera = camera;

            init(initialRobotPos, initialRobotOrientation);
        }

        void init(Vector3 initialRobotPos, double initialRobotOrientation)
        {
            m_RobotPosParticules = new RobotStateParticuleSet();
            for (int I = m_NbRobotPosParts; I > 0; --I)
                m_RobotPosParticules.Add(new RobotStateParticule(new RobotState(initialRobotPos, initialRobotOrientation), 1.0 / m_NbRobotPosParts));

            m_FeaturesParticules = new SortedList<ulong, FeaturePosParticuleSet>();
        }

        public void run(double CRot, double sCRot, double CForward, double sCForward)
        {
            m_RobotPosParticules.Sim(CRot, sCRot, CForward, sCForward);

            List<FeatureReading> Readings = m_Camera.getMesure();
            RobotState RSig = m_RobotPosParticules.Sig;
            double dRSig = RSig.Orientation * RSig.Pos.X * RSig.Pos.Y * RSig.Pos.Z;


            RobotState RobotPosAvg = m_RobotPosParticules.Avg;//new version
            double RobotSig = Math.Sqrt(Math.Pow(m_RobotPosParticules.Sig.Orientation, 2) + Math.Pow(m_RobotPosParticules.Sig.Pos.X, 2) + Math.Pow(m_RobotPosParticules.Sig.Pos.Y, 2) + Math.Pow(m_RobotPosParticules.Sig.Pos.Z, 2));
            double[][][]/*[]*/ PartialProbs = new double[Readings.Count][][]/*[]*/;
            for (int I = Readings.Count - 1; I>= 0; --I)
            {
                if (!m_FeaturesParticules.ContainsKey(Readings[I].Id))
                {
                    FeaturePosParticuleSet newParticles = new FeaturePosParticuleSet(Readings[I], m_RobotPosParticules.Avg, m_FeaturePosResamplingTreshold);
                    m_FeaturesParticules.Add(Readings[I].Id, newParticles);
                    for (int RemainingPartToCreate = m_NbFeaturePosParts; RemainingPartToCreate > 0; --RemainingPartToCreate)
                    {
                        RobotState Chosen = m_RobotPosParticules[(int)(RandomHandler.R.NextDouble() * m_RobotPosParticules.Count)].RobotState;
                        double Orientation = MathHelper.GaussianRandom(RandomHandler.R, Readings[I].OrientationAngle, Math.PI/180) + Chosen.Orientation;
                        double Elevation = MathHelper.GaussianRandom(RandomHandler.R, Readings[I].ElevationAngle, Math.PI/180);

                        Vector3 tPos = new PolarCoord3(Orientation, Elevation, RandomHandler.R.NextDouble() * 100).ToVector3();

                        newParticles.Add(new FeatureParticule(new Feature(new Vector3(tPos.X + Chosen.Pos.X, tPos.Y + Chosen.Pos.Y, tPos.Z + Chosen.Pos.Z), Readings[I].Id), 1.0 / m_NbFeaturePosParts));
                    }
                }

                
                Feature F = m_FeaturesParticules[Readings[I].Id].Sig;
                double FSig = F.Pos.X * F.Pos.Y * F.Pos.Z;
                PartialProbs[I] = new double[m_NbFeaturePosParts][]/*[]*/;
                for (int J = m_NbFeaturePosParts - 1; J >= 0; --J)
                {
                    /*PartialProbs[I][J] = new double[m_NbRobotPosParts][];
                    for (int K = m_NbRobotPosParts - 1; K >= 0; --K)
                    {*/
                        PartialProbs[I][J]/*[K]*/ = new double[2];
                        FeatureParticule CurFeature = m_FeaturesParticules[Readings[I].Id][J];


                        double Orientation = Math.Atan2(CurFeature.Feature.Pos.X - RobotPosAvg.Pos.X, CurFeature.Feature.Pos.Z - RobotPosAvg.Pos.Z) + RobotPosAvg.Orientation;
                        double Elevation = Math.Atan2(CurFeature.Feature.Pos.Y - RobotPosAvg.Pos.Y, Math.Sqrt(Math.Pow(CurFeature.Feature.Pos.Z - RobotPosAvg.Pos.Z, 2) + Math.Pow(CurFeature.Feature.Pos.X - RobotPosAvg.Pos.X, 2)));
                        /*double Orientation = Math.Atan2(CurFeature.Feature.Pos.X - m_RobotPosParticules[K].RobotState.Pos.X, CurFeature.Feature.Pos.Z - m_RobotPosParticules[K].RobotState.Pos.Z) + m_RobotPosParticules[K].RobotState.Orientation;
                        double Elevation = Math.Atan2(CurFeature.Feature.Pos.Y - m_RobotPosParticules[K].RobotState.Pos.Y, Math.Sqrt(Math.Pow(CurFeature.Feature.Pos.Z - m_RobotPosParticules[K].RobotState.Pos.Z, 2) + Math.Pow(CurFeature.Feature.Pos.X - m_RobotPosParticules[K].RobotState.Pos.X, 2)));*/


                        //TODO Voir p(z|...) & p(Particule | z,...)
                        //PartialProbs[I][J][K][0] = MathHelper.Gauss(Readings[I].OrientationAngle, 1, Orientation) * m_RobotPosParticules[K].Weight;
                        //PartialProbs[I][J][K][1] = MathHelper.Gauss(Readings[I].ElevationAngle, 1, Elevation) * m_RobotPosParticules[K].Weight;

                        PartialProbs[I][J]/*[K]*/[0] = MathHelper.GaussAngle(Orientation, .5, Readings[I].OrientationAngle)/* * m_RobotPosParticules.getWeight(K)*/;
                        PartialProbs[I][J]/*[K]*/[1] = MathHelper.GaussAngle(Elevation, .5, Readings[I].ElevationAngle)/* * m_RobotPosParticules.getWeight(K)*/;
                        /*PartialProbs[I][J][K][0] = MathHelper.Gauss(Orientation, .5, Readings[I].OrientationAngle) * m_RobotPosParticules.getWeight(K);
                        PartialProbs[I][J][K][1] = MathHelper.Gauss(Elevation, .5, Readings[I].ElevationAngle) * m_RobotPosParticules.getWeight(K);*/
                    //}
                }
            }

            m_FeaturesParticules = UpdateFeatureParticules.Update2(m_FeaturesParticules, Readings, RobotState, PartialProbs, m_FeaturePosResamplingTreshold, m_NbFeaturePosParts);

            Feature[] FtempAvg = new Feature[Readings.Count];
            double[] FtempSig2 = new double[Readings.Count];
            double[] FtempSig = new double[Readings.Count];
            for (int I = Readings.Count - 1; I >= 0; --I)
            {
                FtempAvg[I] = m_FeaturesParticules[Readings[I].Id].Avg;
                FtempSig2[I] = Math.Pow(m_FeaturesParticules[Readings[I].Id].Sig.Pos.X, 2) + Math.Pow(m_FeaturesParticules[Readings[I].Id].Sig.Pos.Y, 2) + Math.Pow(m_FeaturesParticules[Readings[I].Id].Sig.Pos.Z, 2);
                FtempSig[I] = Math.Sqrt(FtempSig2[I]);
            }

            double CurSig = 0;
            double CurSig2 = 0;
            double CurW;
            for (int K = m_NbRobotPosParts - 1; K >= 0; --K)
            {
                double Sum = 0;
                for (int I = Readings.Count - 1; I >= 0; --I)
                {
                    /*{
                        Feature F = m_FeaturesParticules[Readings[I].Id].Avg;
                    }*/
                    /*for (int J = m_NbFeaturePosParts - 1; J >= 0; --J)
                        Sum += PartialProbs[I][J][K][0] * PartialProbs[I][J][K][1];*/

                    double Orientation = Math.Atan2(FtempAvg[I].Pos.X - m_RobotPosParticules[K].RobotState.Pos.X, FtempAvg[I].Pos.Z - m_RobotPosParticules[K].RobotState.Pos.Z) + m_RobotPosParticules[K].RobotState.Orientation;
                    double Elevation = Math.Atan2(FtempAvg[I].Pos.Y - m_RobotPosParticules[K].RobotState.Pos.Y, Math.Sqrt(Math.Pow(FtempAvg[I].Pos.Z - m_RobotPosParticules[K].RobotState.Pos.Z, 2) + Math.Pow(FtempAvg[I].Pos.X - m_RobotPosParticules[K].RobotState.Pos.X, 2)));


                    //TODO Voir p(z|...) & p(Particule | z,...)
                    //PartialProbs[I][J][K][0] = MathHelper.Gauss(Readings[I].OrientationAngle, 1, Orientation) * m_RobotPosParticules[K].Weight;
                    //PartialProbs[I][J][K][1] = MathHelper.Gauss(Readings[I].ElevationAngle, 1, Elevation) * m_RobotPosParticules[K].Weight;

                    double T1 = MathHelper.GaussAngle(Orientation, FtempSig2[I], Readings[I].OrientationAngle);
                    double T2 = MathHelper.GaussAngle(Elevation, FtempSig2[I], Readings[I].ElevationAngle);


                    CurW = FtempSig[I] / (CurSig + FtempSig[I]);
                    CurSig = CurW * CurW + (1 - CurW) * FtempSig[I];
                    CurSig2 = Math.Pow(CurSig, 2);
                    Sum = CurW * Sum + (1 - CurW) * T1 * T2;
                }


                CurW = RobotSig / (CurSig2 + RobotSig);
                m_RobotPosParticules.UpdateWeight(K, (1 - CurW) * Sum + CurW * m_RobotPosParticules.getWeight(K));
            }

            //m_RobotPosParticules.Normalize();

            //TODO See if Treshold is ok
            if (m_RobotPosParticules.CalcNeff() < m_RobotPosResamplingTreshold)
            {
                m_RobotPosParticules.Normalize();
                m_RobotPosParticules.Resample();
            }
            else
                m_RobotPosParticules.Normalize();
        }
    }
}
