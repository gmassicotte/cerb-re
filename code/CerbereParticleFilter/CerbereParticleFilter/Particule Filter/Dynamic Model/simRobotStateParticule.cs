﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CerbereParticleFilter.Particule_Filter;
using CerbereParticleFilter.BaseTypes;

namespace CerbereParticleFilter.Particule_Filter.Dynamic_Model
{
    public static class simRobotStateParticule
    {
        public static List<RobotStateParticule> simPart(double CRot, double sCRot, double CForward, double sCForward, List<RobotStateParticule> latestRobotStates)
        {
            List<RobotStateParticule> newRobotStates = new List<RobotStateParticule>();
            foreach (RobotStateParticule RSPart in latestRobotStates)
            {
                double Forward = MathHelper.GaussianRandom(RandomHandler.R, CForward, sCForward);
                double Orientation = MathHelper.GaussianRandom(RandomHandler.R, CRot, sCRot);


                double Weight = RSPart.Weight * MathHelper.Gauss(CForward, sCForward, Forward) * MathHelper.Gauss(CRot, sCRot, Orientation);

                RobotState RS = new RobotState(
                    new Vector3(
                        RSPart.RobotState.Pos.X + Forward * Math.Sin(Orientation + RSPart.RobotState.Orientation),
                        RSPart.RobotState.Pos.Y,
                        RSPart.RobotState.Pos.Z + Forward * Math.Cos(Orientation + RSPart.RobotState.Orientation)),
                        
                    Orientation + RSPart.RobotState.Orientation);


                newRobotStates.Add(new RobotStateParticule(RS, Weight));
            }
            return newRobotStates;
        }
    }
}
