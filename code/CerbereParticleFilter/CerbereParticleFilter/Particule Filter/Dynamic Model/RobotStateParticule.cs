﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CerbereParticleFilter.Particule_Filter.Dynamic_Model
{
    public class RobotStateParticule : Particule<RobotState>
    {
        public RobotState RobotState
        {
            get { return m_Data; }
            set { m_Data = value; }
        }


        public RobotStateParticule(RobotState robotState, double weight)
            : base(robotState, weight)
        { }
    }
}
