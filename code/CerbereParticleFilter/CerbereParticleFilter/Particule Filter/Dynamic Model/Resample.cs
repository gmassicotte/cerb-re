﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CerbereParticleFilter.BaseTypes;

namespace CerbereParticleFilter.Particule_Filter.Dynamic_Model
{
    public static class Resample
    {
        public static List<RobotStateParticule> resample(List<RobotStateParticule> latestRobotPosParticles)
        {
            List<RobotStateParticule> newRobotPosParticles = new List<RobotStateParticule>();

            double[] index = new double[latestRobotPosParticles.Count + 1];
            for (int I = 0; I < index.Length-1; ++I)
                index[I + 1] = index[I] + latestRobotPosParticles[I].Weight;
            index[index.Length - 1] = 1;

            List<double> ParticleID = new List<double>();
            for (int I = 0; I < index.Length; ++I)
                ParticleID.Add(RandomHandler.R.NextDouble());
            ParticleID.Sort();

            int IdxList = 0, IdxIndex = 1;
            while (IdxList < index.Length)
            {
                if (ParticleID[IdxList] < index[IdxIndex])
                {
                    newRobotPosParticles.Add(new RobotStateParticule(latestRobotPosParticles[IdxIndex-1].RobotState.clone(), 0));
                    ++IdxList;
                }
                else
                    ++IdxIndex;
            }

            for (int K = newRobotPosParticles.Count - 1; K >= 0; --K)
                newRobotPosParticles[K].UpdateWeight(1.0 / newRobotPosParticles.Count);

            return newRobotPosParticles;
        }
    }
}
