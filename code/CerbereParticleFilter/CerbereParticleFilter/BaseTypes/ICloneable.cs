﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CerbereParticleFilter.BaseTypes
{
    public interface ICloneable<T>
    {
        T clone();
    }
}
