﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CerbereParticleFilter.BaseTypes
{
    public static class IdGenerator
    {
        static ulong CurId = 0;

        public static ulong getID()
        {
            return CurId++;
        }
    }
}
