﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CerbereParticleFilter.BaseTypes
{
    public static class RandomHandler
    {
        public static Random R {  get { return m_r; } }

        static Random m_r = new Random();
    }
}
