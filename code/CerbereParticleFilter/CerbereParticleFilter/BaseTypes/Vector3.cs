﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CerbereParticleFilter.BaseTypes
{
    public class Vector3
    {
        public double X
        {
            get { return m_x; }
            set { m_LengthChanged = true;  m_x = value; }
        }
        public double Y
        {
            get { return m_y; }
            set { m_LengthChanged = true; m_y = value; }
        }
        public double Z
        {
            get { return m_z; }
            set { m_LengthChanged = true; m_z = value; }
        }

        double m_x;
        double m_y;
        double m_z;


        bool m_LengthChanged;
        double m_Length;
        double m_SquareLength;

        public Vector3(double x, double y, double z)
        {
            m_x = x;
            m_y = y;
            m_z = z;
            m_LengthChanged = true;
            m_Length = Length;
        }

        public Vector3(Vector3 rhs)
        {
            m_x = rhs.m_x;
            m_y = rhs.m_y;
            m_z = rhs.m_z;
            m_LengthChanged = rhs.m_LengthChanged;
            m_Length = rhs.m_Length;
            m_SquareLength = rhs.m_SquareLength;
        }

        public static Vector3 operator -(Vector3 lhs, Vector3 rhs)
        {
            return new Vector3(lhs.m_x - rhs.m_x, lhs.m_y - rhs.m_y, lhs.m_z - rhs.m_z);
        }

        public static Vector3 operator +(Vector3 lhs, Vector3 rhs)
        {
            return new Vector3(lhs.m_x + rhs.m_x, lhs.m_y + rhs.m_y, lhs.m_z + rhs.m_z);
        }

        public static Vector3 operator *(double lhs, Vector3 rhs)
        {
            return new Vector3(lhs * rhs.m_x, lhs * rhs.m_y, lhs * rhs.m_z);
        }
        public static Vector3 operator *(Vector3 lhs, double rhs)
        {
            return new Vector3(lhs.m_x * rhs, lhs.m_y * rhs, lhs.m_z * rhs);
        }

        public static Vector3 CrossProduct(ref Vector3 lhs, ref Vector3 rhs)
        {
            return new Vector3(lhs.m_y * rhs.m_z - lhs.m_z * rhs.m_y, lhs.m_z * rhs.m_x - lhs.m_x * rhs.m_y, lhs.m_x * rhs.m_y - lhs.m_y * rhs.m_x);
        }

        public static double DotProduct(ref Vector3 lhs, ref Vector3 rhs)
        {
            return lhs.m_x * rhs.m_x + lhs.m_y * rhs.m_y + lhs.m_z * rhs.m_z;
        }

        public double Length
        {
            get
            {
                if (m_LengthChanged)
                    return (m_Length = Math.Sqrt(SquareLength));
                else
                    return m_Length;
            }
        }

        public double SquareLength
        {
            get
            {
                if (m_LengthChanged)
                {
                    m_LengthChanged = false;
                    return (m_SquareLength = Math.Pow(m_x, 2) + Math.Pow(m_y, 2) + Math.Pow(m_z, 2));
                }
                else
                    return m_SquareLength;
            }
        }

        public Vector3 clone()
        {
            return new Vector3(this);
        }
    }
}
