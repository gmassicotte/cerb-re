﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CerbereParticleFilter.BaseTypes
{
    public class PolarCoord3
    {
        public double OrientationAngle
        {
            get { return m_OrientationAngle; }
            set { m_OrientationAngle = value; }
        }
        public double ElevationAngle
        {
            get { return m_ElevationAngle; }
            set { m_ElevationAngle = value; }
        }

        public double Distance
        {
            get { return m_Distance; }
            set { m_Distance = value; }
        }


        double m_OrientationAngle;
        double m_ElevationAngle;
        
        double m_Distance;

        

        public PolarCoord3(double orientationAngle, double elevationAngle, double distance)
        {
            m_OrientationAngle = orientationAngle;
            m_ElevationAngle = elevationAngle;
            m_Distance = distance;
        }

        public Vector3 ToVector3()
        {
            double HorizontalDist = Math.Cos(m_ElevationAngle) * m_Distance;
            return new Vector3(Math.Sin(m_OrientationAngle) * HorizontalDist,
                Math.Sin(m_ElevationAngle) * m_Distance,
                Math.Cos(m_OrientationAngle) * HorizontalDist);
        }
    }
}
