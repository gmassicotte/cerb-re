﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CerbereParticleFilter.BaseTypes
{
    public static class MathHelper
    {
        public static double Gauss(double mu, double sigma, double x)
        {
            return 1 / Math.Sqrt(sigma * Math.PI * 2) * Math.Exp(-.5 * Math.Pow((x - mu) / sigma, 2));
        }

        public static double GaussianRandom(Random r, double mu, double sigma)
        {
            return Math.Sqrt(-2.0 * Math.Log(r.NextDouble())) * Math.Sin(2.0 * Math.PI * r.NextDouble()) * sigma + mu;
        }

        public static double GaussAngle(double mu, double sigma, double x)
        {
            double diff = Math.IEEERemainder(x -  mu, Math.PI * 2);
            return 1 / Math.Sqrt(sigma * Math.PI * 2) * Math.Exp(-.5 * Math.Pow(diff / sigma, 2));
        }

        public static Vector3 Intersect(Vector3 v1, Vector3 p1, Vector3 v2, Vector3 p2)
        {
            Vector3 Normale = Vector3.CrossProduct(ref v1, ref v2);

            Vector3 P1P2 = p2 - p1;
            Vector3 Dmin = (Vector3.DotProduct(ref P1P2, ref Normale) / Normale.SquareLength) * Normale;
            P1P2 -= Dmin;

            Vector3 P1P3 = P1P2 - (Vector3.DotProduct(ref P1P2, ref v2) / v2.SquareLength) * v2;

            return p1 + P1P3.SquareLength / Vector3.DotProduct(ref v1, ref P1P3) * v1 + .5 * Dmin;
        }
    }
}
