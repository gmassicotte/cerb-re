﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CerbereParticleFilter.BaseTypes;

namespace CerbereParticleFilter
{
    public class World
    {
        public List<Feature> Features
        {
            get { return m_Features; }
            set { m_Features = value; }
        }

        public RobotState RobotState
        {
            get { return m_RobotState; }
            set { m_RobotState = value; }
        }

        List<Feature> m_Features;

        RobotState m_RobotState;


        public World(int nbFeatures, double sizeX, double sizeY, double sizeZ, Vector3 initialPos, double initialOrientation)
        {
            m_Features = new List<Feature>();
            for (int I = nbFeatures; I > 0; --I)
                m_Features.Add(new Feature(new Vector3(RandomHandler.R.NextDouble() * sizeX, RandomHandler.R.NextDouble() * sizeY, RandomHandler.R.NextDouble() * sizeZ)));

            m_RobotState = new RobotState(initialPos.clone(), initialOrientation);
        }
    }
}
