﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CerbereParticleFilter.BaseTypes;

namespace CerbereParticleFilter.Data_Acquisition
{
    public class Capteur
    {
        protected World m_World;
        protected Capteur(World world)
        {
            m_World = world;
        }
    }

    public class CapteurAngle : Capteur
    {
        public int XNbPix
        {
            get { return m_xNbPix; }
            set { m_xNbPix = value; }
        }
        public int YNbPix
        {
            get { return m_yNbPix; }
            set { m_yNbPix = value; }
        }

        public double XAngleMin
        {
            get { return m_xAngleMin; }
            set { m_xAngleMin = value; }
        }
        public double XAngleMax
        {
            get { return m_xAngleMax; }
            set { m_xAngleMax = value; }
        }

        public double YAngleMin
        {
            get { return m_yAngleMin; }
            set { m_yAngleMin = value; }
        }
        public double YAngleMax
        {
            get { return m_yAngleMax; }
            set { m_yAngleMax = value; }
        }

        public double XAngleRes
        {
            get { return m_xAngleRes; }
            set { m_xAngleRes = value; }
        }
        public double YAngleRes
        {
            get { return m_yAngleRes; }
            set { m_yAngleRes = value; }
        }


        int m_xNbPix;
        int m_yNbPix;

        double m_xAngleMin;
        double m_xAngleMax;

        double m_yAngleMin;
        double m_yAngleMax;

        double m_xAngleRes;
        double m_yAngleRes;

        public CapteurAngle(World world, int xNbPix, int yNbPix, double xAngleMin, double xAngleMax, double yAngleMin, double yAngleMax) : base(world)
        {
            m_xNbPix = xNbPix;
            m_yNbPix = yNbPix;

            m_xAngleMin = xAngleMin;
            m_xAngleMax = xAngleMax;

            m_yAngleMin = yAngleMin;
            m_yAngleMax = yAngleMax;

            m_xAngleRes = (xAngleMax - xAngleMin) / xNbPix;
            m_yAngleRes = (yAngleMax - yAngleMin) / yNbPix;
        }

        public List<FeatureReading> getMesure()
        {
            List<FeatureReading> Readings = new List<FeatureReading>();
            foreach (Feature F in m_World.Features)
            {
                Vector3 Pos = F.Pos - m_World.RobotState.Pos;

                double OrientationAngle = Math.Atan2(Pos.X, Pos.Z) + m_World.RobotState.Orientation;
                double ElevationAngle = Math.Atan2(Pos.Y, Math.Sqrt(Math.Pow(Pos.X, 2) + Math.Pow(Pos.Z, 2)));

                OrientationAngle = OrientationAngle / m_xAngleRes;
                ElevationAngle = ElevationAngle / m_yAngleRes;

                OrientationAngle = Math.Round(OrientationAngle);
                ElevationAngle = Math.Round(ElevationAngle);

                OrientationAngle = OrientationAngle * m_xAngleRes;
                ElevationAngle = ElevationAngle * m_yAngleRes;

                if (OrientationAngle > m_xAngleMin && OrientationAngle < m_xAngleMax &&
                    ElevationAngle > m_yAngleMin && ElevationAngle < m_yAngleMax)
                    Readings.Add(new FeatureReading(OrientationAngle, ElevationAngle, F.Id));
            }
            return Readings;
        }
    }
}
